# Deploy

[![Deploy to Azure](https://learn.microsoft.com/en-us/azure/templates/media/deploy-to-azure.svg)](https://portal.azure.com/#create/Microsoft.Template/uri/https%3A%2F%2Feks2-d9a200b1fb3808883601a49544b47ba7.s3.amazonaws.com%2FuserManagedIdentity.json)

[![Deploy to Azure](https://learn.microsoft.com/en-us/azure/templates/media/deploy-to-azure.svg)](https://portal.azure.com/#create/Microsoft.Template/uri/https%3A%2F%2Feks2-d9a200b1fb3808883601a49544b47ba7.s3.amazonaws.com%2FuserManagedIdentityTenant.json)

_that .s3.amazonaws nonsense is because they actually **CORS** it in from the Portal's tab_


# Mental Model

```plantuml
@startuml
skinparam componentStyle rectangle

package "Tenant" {
  package "Management Group" {
    package "Subscription" {
    [User Managed Identity] ..> [Federated Credential] : trusts
     note right of [Federated Credential]
     the OIDC JWKS
     //e.g.// https://oidc.eks.amazonaws.com/id/CAFEBABE
     end note
    }
    note top of "Subscription"
    these are where "billable" things are instantiated
    and closely map to a GCP "Project"
    end note
  }
  note top of "Management Group"
  these can be further nested and are optional since
  Subscription can exist directly in the Tenant
  (as it is strictly speaking a Management Group itself)
  end note
  interface Reader
  [Role Assignment] - [User Managed Identity] : assignee
  [Role Assignment] - Reader : assignsTo
}
@enduml
```
