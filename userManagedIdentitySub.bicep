targetScope = 'subscription'

@description('the U.M.I. **principalId**')
param userManagedIdentityId string

/** https://learn.microsoft.com/en-us/azure/role-based-access-control/built-in-roles#reader */
var BuiltInRole_Reader_Id = 'acdd72a7-3385-48ef-bd42-f606fba81ae7'

/**
I have no idea why this is required
https://learn.microsoft.com/en-us/azure/role-based-access-control/built-in-roles/storage#storage-blob-data-reader
*/
var BuiltInRole_BlobReader_Id = '2a2b9908-6ea1-4ae2-8e65-a410df84e7d1'
/** https://learn.microsoft.com/en-us/azure/role-based-access-control/built-in-roles/storage#storage-file-data-privileged-reader */
var BuiltInRole_FileReader_Id = 'b8eda974-7b85-4f76-af95-65846b26df6d'

var RoleIds = [
  BuiltInRole_Reader_Id
  BuiltInRole_BlobReader_Id
  BuiltInRole_FileReader_Id
]

resource assignRoles 'Microsoft.Authorization/roleAssignments@2022-04-01' = [for id in RoleIds : {
  scope: subscription()
  name: guid(tenant().tenantId, subscription().subscriptionId, userManagedIdentityId, id)
  properties: {
    description: 'allow Open Raven to read Azure resources in your Tenant'
    principalId: userManagedIdentityId
    roleDefinitionId: tenantResourceId('Microsoft.Authorization/roleDefinitions', id)
    principalType: 'ServicePrincipal'
  }
}]

// there are no outputs for this, we already have all that we need
